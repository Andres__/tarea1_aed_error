#include <iostream>
#include <cstdlib>

using namespace std;


struct Nodo{
    string cont;
    Nodo* siguiente;
};
Nodo* head = NULL;
void quitarUltimo()
{
   if (head->siguiente== NULL)
   {
        head = NULL;
   }
   else
   {
        Nodo* hed = head;
        while (hed->siguiente->siguiente != NULL)
            hed = hed->siguiente;
            Nodo* ultimo = hed->siguiente;
            hed->siguiente = NULL;
            free(ultimo);   
   }
}
void quitarPrimero()
{   
    Nodo * hed;
    hed = head;
    head = head->siguiente;
    free(hed);
}
void contenido(Nodo *head, int opcion)
{
    if (head == NULL)
    {
        cout << "No es posible eliminar el nodo, la lista está vacía" << endl;
    }
    else 
    {   
        if (opcion == 3)
        {
            quitarUltimo();
        }
        else 
        {
            quitarPrimero();
        }
    }
}
// mostrar valores de la lista 
void imprimirLista(Nodo *head)
{
    if (head != NULL)
    {
    cout << "El contendido de la lista es :" << endl;
    while (head != NULL)
    {
        cout<<" "<<head->cont;
        head = head->siguiente;
    }
    }

    else {
        cout << "no hay nada en la lista" << endl;
    }
}
// agregar primer nodo indican
void insertarPrimero(Nodo** head, string nuevo_Estudiante)
{   

    Nodo* newNodo = new Nodo();

    newNodo->cont = nuevo_Estudiante;
 
    newNodo->siguiente = *head;
    *head = newNodo;
}
///
void buscarEstudiante(Nodo *head, string name)
{   
    Nodo * hed = head;
    bool existe = false;

    while (hed != NULL)
    {
        if (hed->cont == name)
        {
            existe = true;
            break;

        }
        else
        {
            hed = hed->siguiente; 
        }

    }
    if (existe == true)
    {
        cout << "El nombre del estudiante ingresado se encuentra en la lista :)";
    }
    else 
    {
        cout << "El nombre del estudiante ingresado NO se encuentra en la lista";
    }
    
}

int main()
{
    int opcion;
    bool funciona = true;
    
    string aux = "";
    string buscar = "";
    string name = "";

    do {
    
        
        // Texto del menú que se verá cada vez
        cout << "\n\nMenu de Opciones" << endl;
        cout << "1. Opcion 1: Buscar estudiante " << endl;
        cout << "2. Opcion 2: Insertar nodo" << endl;
        cout << "3. Opcion 3: Eliminar último nodo de lista" << endl;
        cout << "4. Opcion 4: Eliminar primer nodo de lista" << endl;
        cout << "5. Opcion 5: Imprimir contenido de la lista" << endl;
        cout << "0. SALIR" << endl;
        
        cout << "\nIngrese una opcion: ";
        cin >> opcion;
        
        switch (opcion) {
            case 1:
                // Busqueda de estudiante

                cout << "Ingrese el nombre del estudiante que desee buscar: ";
                cin >> name;
                buscarEstudiante(head, name);
                break;
                
            case 2:
                // Insertar nodo al inicio
                if (head == NULL)
                {
                    cout << "Lista vacía, ingrese nombre del primer estudiante: ";
                }
                else 
                {
                    cout << "Ingrese el nombre del siguiente estudiante: ";
                }
                cin >> aux;
                insertarPrimero(&head, aux); 
                break;
                
            case 3:
                // Eliminar ultimo nodo
                cout << "Se ha eliminado el ultimo estudiante (nodo)"<< endl;
                contenido(head, opcion);
                break;
                
            case 4:
                // Eliminar primer nodo
                cout << "Se ha eliminado el primer estudiante (nodo)"<< endl;
                
                contenido(head, opcion);
                break;
            
            
            case 5:
                // Imprimir lista (si hay)
                imprimirLista(head);

                break;

            case 0:
            	funciona = false;
            	break;
        }
        aux = "";
    } while (funciona);

    return 0;
}

